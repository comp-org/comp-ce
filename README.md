# COMP Community Edition (CE)

**COMP CE** is an open-source utility for sharing computational models.

To see COMP CE in action, visit [compmodels.org](https://www.compmodels.org).

Features:
- Share public models and simulations for free.
- Pay for your own compute at cost.
- Sponsor compute for others if you'd like.

Under development:

- Share non-tabular output.

Planned:
- REST API to run simulations and build GUI simulation pages.

## Publish

Publish models to the public at www.compmodels.org/publish.

## Contributing

COMP CE is an open source project and anyone can contribute code or suggestions.

You can reach COMP developers to discuss how to get started by opening an issue or joining the COMP Community [chat room](https://riot.im/app/#/room/!WQWxPnwidsSToqkeLk:matrix.org).

## License

COMP Community Edition is licensed under the open source [GNU Affero General Public License](/License.txt) to Compute Tooling, LLC.

## Local Installation

The technical documentation is out of date and will be updated once [#127](https://github.com/comp-org/comp-ce/pull/127) has been merged.
